<?php

namespace App\Delivery;

use App\Delivery\Exceptions\InvalidClassException;

/**
 * Class Delivery
 *
 * @package App
 */
class Delivery
{
    private int $weight;

    public function setWeight(int $weight): Delivery
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * @param string $carrier
     *
     * @return mixed
     * @throws InvalidClassException
     */
    public function calculate(string $carrier)
    {
        $carrier = $this->toCamel($carrier);

        $class = "\\App\\Delivery\\Carrier\\{$carrier}";
        if (!class_exists($class)) {
            throw new InvalidClassException("Failed to initialize the class $carrier");
        }

        return (new $class($this->weight))->priceCalculation();
    }

    /**
     * @param string $input
     *
     * @return string
     */
    private function toCamel(string $input): string
    {
        return str_replace(' ', '', ucwords(str_replace('_', ' ', $input)));
    }
}
