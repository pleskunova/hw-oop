<?php

namespace App\Delivery;

/**
 * Interface CarrierInterface
 *
 * @package App\Delivery
 */
interface CarrierInterface
{
    /**
     * @return int
     */
    public function priceCalculation(): int;
}
