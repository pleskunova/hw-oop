<?php

namespace App\Delivery\Carrier;

use App\Delivery\CarrierInterface;

/**
 * Class Dhl
 *
 * @package App
 */
class Dhl extends BaseCarrier implements CarrierInterface
{
    /**
     * @return int
     */
    public function priceCalculation(): int
    {
        return $this->weight * 100;
    }
}
