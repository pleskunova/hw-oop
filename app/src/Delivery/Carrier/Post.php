<?php

namespace App\Delivery\Carrier;

use App\Delivery\CarrierInterface;

/**
 * Class Post
 *
 * @package App
 */
class Post extends BaseCarrier implements CarrierInterface
{
    /**
     * @return int
     */
    public function priceCalculation(): int
    {
        if ($this->weight <= 10) {
            return $this->weight * 100;
        }

        return $this->weight * 1000;
    }
}
