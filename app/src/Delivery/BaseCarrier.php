<?php

namespace App\Delivery\Carrier;

/**
 * Class BaseCarrier
 *
 * @package App
 */
abstract class BaseCarrier
{
    protected int $weight;

    public function __construct(int $weight)
    {
        $this->weight = $weight;
    }
}
