<?php

namespace App\Delivery;

use App\Delivery\Exceptions\InvalidClassException;

require __DIR__ . '/vendor/autoload.php';

$delivery = new Delivery();

try {
    echo $delivery->setWeight(5)->calculate('dhl');
    echo '<br />';
    echo $delivery->setWeight(10)->calculate('post');
} catch (InvalidClassException $e) {
    echo '<pre>' . print_r($e->getMessage(), true) . '</pre>';
}
